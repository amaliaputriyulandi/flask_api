from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello'

@app.route('/calc')
def calc():
    a = int(request.args.get('a')) # ke int agar bisa di +
    b = int(request.args.get('b')) # nnti req kaya gini http://localhost:5000/calc?a=5&b=4

    return str(sum(a,b)) # diubah ke str lagi karena gabisa kalau response nya int

def sum(a,b):
    return a + b